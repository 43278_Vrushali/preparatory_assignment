/* 7. Implement doubly linked list of bank accounts. Each account has information including id, type, 
balance and account holder. The account holder details include name, address & contact details. 
Write a menu-driven program to implement add first, add last, display all (forward), display all (backword), 
find by account id, find by account holder name, delete all functionalities. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct account{
    int id;
    char acc_holder[40];
    char type[30];
    double balance;    
}account_t;

//------------------------------------------------------------------
//Output #Account_display
void account_display(account_t *a) {
	printf("| id : %d,\t Account Holder : %s,\t Type : %s,\t Balance : %2lf |\n", a->id, a->acc_holder, a->type,a->balance);
}

typedef struct node
{
    account_t data;
    struct node *next;
    struct node *prev;    
}node_t;

node_t *next = NULL;
node_t *prev = NULL;
node_t *head = NULL;
node_t *last = NULL;

//------------------------------------------------------------------
//Logic #Accept Data
account_t accept_data(){
    account_t acc;
    printf("Enter Id : ");
    scanf("%d",&acc.id);
    
    printf("Enter Account holder name : ");
    scanf("%s",&acc.acc_holder);

    printf("Enter Account Type : ");
    scanf("%s",&acc.type);

    printf("Enter Balance : ");
    scanf("%lf",&acc.balance);

    return acc;
}

//------------------------------------------------------------------
//Logic #Create node
node_t* create_node(account_t acc) {
	node_t *newnode = (node_t *)malloc(sizeof(account_t));
	newnode->data = acc;
	newnode->next = NULL;
    newnode->prev = NULL;
	return newnode;
}

//------------------------------------------------------------------
//Logic #Add First
void add_first(){
    // 1. Check if list is empty
    if(head == NULL){
        // 2. if yes take data from user
        head = (node_t *)malloc(sizeof(account_t));

        printf("Enter data of 1 node: \n");
        account_t acc = accept_data();
        node_t *newnode = create_node(acc);
        //3. Assign prev and next of new node to NULL
        newnode->prev = NULL;
        newnode->next = NULL;
        // 4. Assign head to new node
        head = newnode;
        // 5. Assign last to new node
        last = head;   

        printf("New Node Added Successfully ...!");
    }       
    else
    {
        // 2. if no take data from user
        head = (node_t *)malloc(sizeof(account_t));

        printf("Enter data of 1 node: \n");
        account_t acc = accept_data();
        node_t *newnode = create_node(acc);
        
        printf("Newnode added");
        account_display(&newnode->data);

        //4. Assign prev and next of new node to NULL
        newnode->next = head;
        newnode->prev = NULL;        

        //3. Assign prev head to new node
        head->prev = newnode;
        
        // 5. Assign head to new node
        head = newnode;  
        printf("New Node Added Successfully ...!"); 
    }
    
}

//------------------------------------------------------------------
//Logic #Add Data
void add_last(){
    node_t *newnode;
    // 1. Check if list is empty
    if(last == NULL)
        printf("List is empty");
    else
    {
        // 2. If not take data from user
        newnode = (node_t *)malloc(sizeof(account_t));

        printf("Enter data for last node: \n");
        account_t acc = accept_data();
        node_t *newnode = create_node(acc);

        // 3. Update prev of newdata to last
        newnode->next = NULL;
        newnode->prev = last;        
        
        // 4. Update next of last to new data
        last->next = newnode;

        // 5. assign newdata as last
        last = newnode;

        printf("Node inserted successfully...!");
    }
    
}

//------------------------------------------------------------------
//Output #display_list
void display_list_forward(){
    node_t *temp;
    
    if(head == NULL)
        printf("List is empty");
    else
    {
        temp = head;
        while (temp != NULL)
        {
            account_display(&temp->data);
            temp = temp->next;
        }
        
    }    
}

//------------------------------------------------------------------
//Logic #Find account by name
void find_acc_by_name(char name[]){
    node_t *temp;
    int found=0;
    printf("Name : %s",name);
    // 1. Check if list is empty
    if(head == NULL)
        printf("List is empty");
    else
    {
        temp = head;
        
        // 2. Find specified account by name
        while (temp != NULL)
        {
            if(strcmp(name, temp->data.acc_holder) == 0){
                printf("Name : %s\t AccName : %s \n",&name,&temp->data.acc_holder);
                found =1;
                printf("Account Holder details found\n");
                account_display(&temp->data);
                temp = temp->next;
                break;
            }           
        }      
        if(!found)  
            printf("No Account Found...!");
    }    
}

//------------------------------------------------------------------
//Output #display_list_backward
void display_list_backward(){
    node_t *temp;    
    if(head == NULL)
        printf("List is empty");
    else
    {
        temp = last;
        while (temp != NULL)
        {
            account_display(&temp->data);
            temp = temp->prev;
        }
        
    }
    
}

//------------------------------------------------------------------
//Logic #Find account by id
void find_acc_by_id(int id){
    node_t *temp;
    int found=0;
    // 1. Check if list is empty
    if(head == NULL)
        printf("List is empty");
    else
    {
        temp = head;
        // 2. Find specified account by id
        while (temp != NULL)
        {
            if(id == temp->data.id){
                printf("Id : %d\t AccId : %d \n",&id,&temp->data.id);
                found =1;
                printf("Account Holder details found\n");
                account_display(&temp->data);
                break;
            }           
        }      
        if(!found)  
            printf("No Account Found...!");
    }    
}

int main(){
    int choice,id;
    char name[40];
	do {
        printf("\n************** DOUBLY LINKED LIST : ASSIGN 07 **************\n");
		printf("\n\n0. Exit\n1. Add First\n2. Add Last\n3. Display All(Forward)\n4. Display All(Backward)\n");
        printf("5. Find by Account holder name\n6. Find account by id\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Add First
                add_first();
				break;
			case 2:// Add Last
                add_last();
				break;
			case 3:// Display All(Forward)
                display_list_forward();
				break;
            case 4: // Display All(Backward)
                display_list_backward();
				break;
			case 5:// Find by Account holder name
                printf("Enter Account Holder Name to find : ");
                scanf("%s",&name);
                find_acc_by_name(name);
				break;
			case 6:// Find account by id
                printf("Enter Account Holder Id to find : ");
                scanf("%d",&id);
                find_acc_by_id(id);
				break;
        }
    }while (choice != 0);
    return 0;
}


