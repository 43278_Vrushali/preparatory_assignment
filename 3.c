#include <stdio.h>
#include <stdlib.h>

void sum(int a[]) {
    // Declaration 
    int i,sum = 0;

    // Logic #sum_of_element
    for(i=0;i<10;i++) {
        sum += a[i];
    }

    // Output #sum
    if(sum != 0)
        printf("Sum is %d\n",sum);
    else 
        printf("please add member to array\n");
}

void min(int a[]) {
    // Declaration
    int i,min = 0,in;

    // Logic #min_in_array
    for(i=0;i<10;i++) {
        if (a[i]==0)
            continue;
        if (a[i] != 0) {
            min = a[i];
            in = i;
        }
        if(min > a[i]) {
            min = a[i];
            in = i;
        }
    } 

    // Output #min_no
    if(min != 0)
        printf("Minumun number in array is %d at %d index\n",min,in);
    else 
        printf("please add member to array\n");
}

void max(int a[]) {
    // Declaration
    int i,max = 0,in;

    // Logic #max_in_array
    for(i=0;i<10;i++) {
        if (a[i]==0)
            continue;
        if (a[i] != 0) {
            max = a[i];
            in = i;
        }
        if(max < a[i]) {
            max = a[i];
            in = i;
        }
    } 

    // Output #max_no
    if(max != 0) 
        printf("Maximun number in array is %d at %d index.",max,in);
    else
        printf("please add member to array\n");
}

void delete(int a[]) {
    // declaration
    int i,num,choice,ava[10];

    Z:
    // Output #declared_index
    printf("Avaliable Indexes for delete :\n\n");
    printf("a[index] = value\n\n");
    for(i=0;i<10;i++) {
        if(a[i]!=0) {
            ava[i]=i;
            printf("a[%d] = %d\n",i,a[i]);
        }
    }
    printf("\n\n");

    // Output #desired_poisition
    printf("Enter position for delete number : ");
    scanf("%d",&choice);

    // Logic #adding_number_to_array_if_valid
    if(a[choice]!=0) {
        for(i=0;i<10;i++){
            if(choice == ava[i]) {
                // Logic #assign_value_to_poisition
                a[choice] = 0;
                printf("Deleated Sucessfully.\n");
            }
        }
    }
    else {
        printf("selcect avaliable index.\n\n");
        goto Z;
    }

}

void add(int a[])
{
    // Delcaration
    int i,choice,num,ava[10];

    B: 
    // Output #avaliable_index
    printf("Avaliable Indexes :\n");
    for(i=0;i<10;i++) {
        if(a[i]==0) {
            ava[i]=i;
            printf("%d ",i);
        }
    }
    printf("\n\n");

    // Output #desired_poisition
    printf("Enter position for adding number : ");
    scanf("%d",&choice);

    // Input #number
    printf("\nEnter Number :");
    scanf("%d",&num);

    // Logic #adding_number_to_array_if_valid
    if(a[choice]==0 && num > 0) {
        for(i=0;i<10;i++){
            if(choice == ava[i] && num > 0) {
                // Logic #assign_value_to_poisition
                a[choice] = num;
                printf("Addeded Sucessfully.\n");
            }
        }
    }
    else {
        printf("\nYou entere positive number or selcect avaliable index.\n\n");
        goto B;
    } 
}

int main(void)
{
    // Declaration 
    int choice,a[10]={};

    do {
        printf("\n\n0. Exit\n1. Add number\n2. Delete number\n3. Maximum number\n4. Minimun number\n5. Sum of number\nEnter Choice : ");
        scanf("%d",&choice);
        switch(choice) {
            case 1 : // Add number
                add(a);
                break;
            case 2 : // Delete number
                delete(a);
                break;
            case 3 : // Maximun number
                max(a);
                break;
            case 4 : // Minimun number
                min(a);
                break;
            case 5 : // Sum of number
                sum(a);
                break;
        }
    }while(choice != 0);

    return 0;
}