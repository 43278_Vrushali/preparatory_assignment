#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Declaraing #db
#define MOVIE_DB "movies.csv"

// Declaration #struct_movie
typedef struct movie {
    int id;
    char name[80];
    char genres[220];
}movie_t;

// Logic #linked_list
typedef struct node {
    movie_t data;
    struct node *next;
}node_t;

// Global_declaraton #first_node_of_linked_list_initally_start_with_NULL
node_t *head = NULL;

// Logic #create_node
node_t* create_node(movie_t val) {
    // Declaration
    node_t *newnode = (node_t*)malloc(sizeof(node_t));

    // Intialization
    newnode->data = val;
    newnode->next = NULL;

    return newnode;
}

// Logic #add_movie
void add_last(movie_t val) {
    // Declaration
    node_t *newnode = create_node(val);

    // Logic #traversing
    if(head == NULL)
        head = newnode;
    else {
        node_t *trav = head;
        while(trav->next != NULL) 
            trav = trav->next;
        trav->next = newnode;
    }
}

// Output #movie
void movie_display(movie_t *m) {
    printf("id=%d, names=%s, geners=%s\n",m->id,m->name,m->genres);
}

// Logic #display_list
void display_list() {
    node_t *trav = head;
    while(trav != NULL) {
        movie_display(&trav->data);
        trav = trav->next;
    }
}

int parse_movie(char line[], movie_t *m) {
    // Declaration
    int sucess = 0;
    char *id, *name, *genres;

    // Read #one_by_one
     // Logic #first_token
     id = strtok(line, ",\n");
     // Logic #sub-sequent_tokens
     name = strtok(NULL, ",\n");
     genres = strtok(NULL, ",\n");

    if(id == NULL || name == NULL || genres == NULL) 
        sucess = 0;
    else {
        sucess = 1;
        
        // assign #token_to_structure_members
        m->id = atoi(id);
        strcpy(m->name,name);
        strcpy(m->genres, genres);
    }

    return sucess;
}

// Logic #read_line_one_by_one
void load_movies() {
    // Declaration 
    FILE *fp;
    char line[1024];
    movie_t m;

    // Logic #opening_file
    fp = fopen(MOVIE_DB,"r");
    if(fp == NULL) {
        perror("failed to open movies file.\n");
        exit(1);
    }

    // Logic #read_line_one_by_one
    while(fgets(line, sizeof(line), fp) != NULL) {
        //printf("%s",line);
        // Logic #parseing_movies
        parse_movie(line, &m);

        // Output #movies
        //movie_display(&m);

        // Logic #add_to_linklist
        add_last(m); 
    }

    // Logic #close_file
    fclose(fp);
}

// Logic #search_movie_by_name
void find_movie_by_name() {
    // Declaration
    char name[80];
    node_t *trav = head;
    int found = 0;

    // Input #movie_name
    printf("Enter movie name to be searched : ");
    gets(name);

    // Logic #searching_movie_by_name
    trav = head;
    while(trav != NULL) {
        if(strcmp(name, trav->data.name) == 0) {
            movie_display(&trav->data);
            found = 1;
            break;
        }
        trav = trav->next;
    }

    // Output #if_not_found
    if(!found)
        printf("movie not find.\n");

}

// Logic #search_movie_by_gener
void find_movie_by_gener() {
    // Declaration
    char gener[80];
    node_t *trav = head;
    int found = 0;

    // Input #movie_gener
    printf("Enter gener to be searched : ");
    gets(gener);

    // Logic #searching_movie_by_gener
    trav = head;
    while(trav != NULL) {
        if(strstr(trav->data.genres, gener) != NULL) {
            movie_display(&trav->data);
            found = 1;
        }
        trav = trav->next;
    }

    // Output #if_not_found
    if(!found)
        printf("movie not find.\n");
}

int main(void) {
    // calling #load_movies()
    load_movies();

    // output #link_list
    display_list();

    // Logic #find_by_name
    find_movie_by_name();

    // Logic #find_movie_gener
    find_movie_by_gener();

    return 0;
}