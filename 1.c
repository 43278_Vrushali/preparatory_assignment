#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
  // Declaration
  char str[80];
  const char s[2] = ",";
  char *token;

  // Input #string
  scanf("%s", str);

  // Logic #first_token
  token = strtok(str, s);

  // Logic #seprate_charecter
  while( token != NULL ) {
    printf( "%s\n",token); 
    token = strtok(NULL, s);
  }

  return 0;
}