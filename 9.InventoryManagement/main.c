#include <stdio.h>
#include <string.h>
#include "inventory.h"

void sign_in() {
	// find the user in the users file by email 
	// check input password is correct.
	// if correct, call user_area() based on its role.

    // Declaration
    char email[30], password[10];
	user_t u;
	int invalid_user = 0;

	// input #email and password
	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);

	// Logic #finding_user_in_db_by_email
	if(user_find_by_email(&u, email) == 1) {
        // Logic #password_verification
		if(strcmp(password, u.password) == 0) {
            // Logic #owner_or_not
			if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);

			// Logic #call_user_area()_based_on_role.
			if(strcmp(u.role, ROLE_OWNER) == 0)
				owner_area(&u);
			else
				invalid_user = 1;
		}
		else
			invalid_user = 1;
	}
	else
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");
}

void sign_up() {
	// input user details from the user.
    // Testing #adding_user_to_db
       user_t u;
       user_accept(&u);

	 //write user details into the users db.
      // Testing #add_user_in_db
       user_add(&u);
}

int main(){
     // Declaration
    int choice;
	
    owner_area();
    /*// Output #main_menu
	do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // Sign In
			sign_in();
			break;
		case 2:	// Sign Up
			sign_up();	
			break;
		}
	}while(choice != 0);*/
     return 0;
}