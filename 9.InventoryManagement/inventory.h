#ifndef _INVENTORY_H
#define _INVENTORY_H

 // Defineing #owner info
 #define EMAIL_OWNER "vrushali@gmail.com"

 // Declare #member_role
 #define ROLE_OWNER "owner"

// Declare #Category
 #define STATIONARY "stationary"
 #define GROCERY "grocery"
 
 // Declarations #db_details
 #define USER_DB "users.db"
 #define INVENTORY_DB "inventory.db"
 #define TMP_INVENTORY_DB "tmp_inventory.db"

 typedef struct user {
     int id;
	 char name[30];
	 char email[30];
	 char phone[15];
	 char password[10];
	 char role[15];
 }user_t;

// user functions
//void owner_area(user_t *u);
void owner_area();
 void user_accept(user_t *u);
 void user_display(user_t *u);
 int get_next_user_id();

 typedef struct inventory
 {
     int id;
     char itemName[50];
     double price;
     char category[20];
     int quantity;
 }inventory_t;
 
// inventory functions 
 void inventory_accept(inventory_t *i);
 void inventory_display(inventory_t *i);
 int get_next_inventory_id();

 void inventory_add();
 void view_all_inventories();
 void inventory_find_by_id();
 void inventory_edit_id();
 void delete_inventory_item();
 

//Common functions
int user_find_by_email(user_t *u, char email[]);
void user_add(user_t *u);

#endif