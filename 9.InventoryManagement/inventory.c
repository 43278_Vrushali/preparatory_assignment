#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "inventory.h"

// Output #inventory_owner_area
//void owner_area(user_t *u) {
void owner_area() {
	// Declaration 
    int choice,id;
    // Logic #inventory_owner_menu
    do{
        printf("************** INVENTORY MANAGEMENT : WELCOME  **************");
        printf("\n\n0. Sign Out\n1. Add Items\n2. Find Item\n3. Display Items\n4. Edit Items\n5. Delete Item\nEnter Your choice : ");
        scanf("%d",&choice);
        switch (choice)
        {
        case 1 :// Add Items
                inventory_add();
                break;
        case 2 :// Find Item
                inventory_find_by_id();
                break;
        case 3 ://Display Items
                view_all_inventories();
                break;
        case 4 ://Edit Items
                printf("Edit Item");
                inventory_edit_id();
                break;
        case 5 ://Delete Item
                delete_inventory_item();
                break;
        }


    }while (choice != 0);	
}

//-----------------------------------------------------
// Logic #delete_inventory_item
void delete_inventory_item(){
    FILE *fp,*fp_tmp;
    int found=0,id;
    inventory_t i;

    // Logic #open_inventory_file
	fp = fopen(INVENTORY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open inventory file");
		exit(1);
	}

    // Logic #open temp file
    fp_tmp = fopen(TMP_INVENTORY_DB,"wb");
    if(!fp_tmp){
        perror("cannot open tmp_inventory file");
		exit(1);
    }

    // input #inventory_id
	printf("Enter Inventory Id: ");
	scanf("%d", &id);

    // Logic #read_record_one_by_one
	while(fread(&i, sizeof(inventory_t), 1, fp) > 0) {
		if(id == i.id) {
			printf("A record with requested id found and deleted.\n\n");
			found=1;
		}
        else
            fwrite(&i,sizeof(inventory_t),1,fp_tmp);
	}

    if (! found) {
		printf("No record(s) found with the requested id: %d\n\n", id);
	}

	fclose(fp);
	fclose(fp_tmp);

	remove(INVENTORY_DB);
	rename(TMP_INVENTORY_DB, INVENTORY_DB);



}

//-----------------------------------------------------
// Logic #edit_inventory_by_id
void inventory_edit_id(){
	int id, found = 0;
	FILE *fp;
	inventory_t i;

	// input #inventory_id
	printf("Enter Inventory Id: ");
	scanf("%d", &id);

	// Logic #open_inventory_file
	fp = fopen(INVENTORY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open inventory file");
		exit(1);
	}

	// Logic #read_record_one_by_one
	while(fread(&i, sizeof(inventory_t), 1, fp) > 0) {
		if(id == i.id) {
			found = 1;
			break;
		}
	}
	
	if(found) {
		// input #new_book_details 
		long size = sizeof(inventory_t);
		inventory_t ni;
		inventory_accept(&ni);
		ni.id = i.id;

		// Logic #db_pointer_behind_read_record
		fseek(fp, -size, SEEK_CUR);
		// Logic #overwrite_db
		fwrite(&ni, size, 1, fp);
		printf("Inventory updated.\n");
	}
	else 
		printf("Inventory not found.\n");
	fclose(fp);
}

//-----------------------------------------------------
// Logic #find_inventory_by_id
 void inventory_find_by_id(){
     int found=0,id;
     inventory_t i;
     FILE *fp;
    // Logic #open_file
	fp = fopen(INVENTORY_DB, "rb");
     //Open Inventory file
     if(fp == NULL){
        perror("cannot open inventory file\n");
		exit(1);
     }

        printf("Enter Inventory Id : ");
		scanf("%d", &id);
				
    // Logic #read_record_from_db_one_by_one
	while(fread(&i, sizeof(inventory_t), 1, fp) > 0) {
       // Logic #matching_name_partially
		if(id == i.id) {
           found = 1;
		    inventory_display(&i);
		}
	}
    fclose(fp);
    // Logic #nothing_found
	if(!found)
		printf("No such inventory found.\n");
 }
//-----------------------------------------------------

// Output #display_all_inventory
void view_all_inventories(){
    FILE *fp;
	inventory_t i;
	// open inventory file.
	fp = fopen(INVENTORY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open inventory file\n");
		exit(1);
	}
	while((fread(&i , sizeof(i),1, fp) > 0 ))
		inventory_display(&i);

	fclose(fp);
}

//-----------------------------------------------------

// Logic #adiing_inventory_to_db
void inventory_add() {
	FILE *fp;
	// input #inventory_details
	inventory_t i;
	inventory_accept(&i);
	 // Logic open_inventory_db
	  fp = fopen(INVENTORY_DB, "ab");
	  if(fp == NULL) {
	    	perror("cannot open inventory file");
		    exit(1);
	    }
	// Logic #append_book_to_db
	fwrite(&i, sizeof(inventory_t), 1, fp);
	printf("Inventory added in file.\n");
	// close inventory file.
	fclose(fp);
}