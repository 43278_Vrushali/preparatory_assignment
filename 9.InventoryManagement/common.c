#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "inventory.h"

//-----------------------------------------------------

// Logic #adiing_user_to_db
 void user_add(user_t *u) {
	// Logic #open_file_for_appending_data
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}
	
	// Logic #write_data_into_file
	fwrite(u, sizeof(user_t), 1, fp);
	printf("user added into file.\n");
	
	fclose(fp);
}

//-----------------------------------------------------
 // Input #user_info
 void user_accept(user_t *u) {
     u->id = get_next_user_id();

	 printf("name: ");
	 scanf("%s", u->name);

	 printf("email: ");
	 scanf("%s", u->email);

	 printf("phone: ");
	 scanf("%s", u->phone);

	 printf("password: ");
	 scanf("%s", u->password);
     
     // Logic #acessing_only 
	 strcpy(u->role, ROLE_OWNER);
 }

 // Output #user_info
 void user_display(user_t *u) {
     printf("ID : %d\t, NAME : %s\t, EMAIL : %s\t, PHONE : %s\t, ROLE : %s\n", u->id, u->name, u->email, u->phone, u->role);
 }

//-----------------------------------------------------

// Logic #genrating_next_user_id
int get_next_user_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;

	// Logic #open_file
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Logic #mx_id
		max = u.id;
	
	fclose(fp);

	return max + 1;
}
 //-----------------------------------------------------

// Logic #finding_user_by_email
int user_find_by_email(user_t *u, char email[]) {
	FILE *fp;
	int found = 0;

	// Logic #open_file_for_appending_data
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}

	// Logic #read_users_one_by_one
	while(fread(u, sizeof(user_t), 1, fp) > 0) {
        if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	fclose(fp);

	return found;
}

//-----------------------------------------------------

// Input #Inventory_item_info
void inventory_accept(inventory_t *i){
    i->id = get_next_inventory_id();
    printf("Enter ItemName : ");
    scanf("%s",&i->itemName);

    printf("Enter Price : ");
    scanf("%lf",&i->price);

    printf("Enter Category[s/g] : ");
    scanf("%s",&i->category);

    if(strcmp(i->category,"s"))
        strcpy(i->category, STATIONARY);
    else
        strcpy(i->category, GROCERY);

    printf("Enter Quantity : ");
    scanf("%d",&i->quantity);
}

// Output #Inventory_display_info
void inventory_display(inventory_t *i){
    printf("ID : %d,\tItemName : %s,\tPrice : %.2lf,\tCategory : %s,\tQuantity : %d\n",i->id,i->itemName,i->price,i->category,i->quantity);
}

//-----------------------------------------------------

// Logic #genrating_next_inventory_id
int get_next_inventory_id(){
    FILE *fp;
	int max = 0;
	int size = sizeof(inventory_t);
	inventory_t i;

	// Logic #open_file
	fp = fopen(INVENTORY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&i, size, 1, fp) > 0)
		// Logic #mx_id
		max = i.id;
	
	fclose(fp);

	return max + 1;
}