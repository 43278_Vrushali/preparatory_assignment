#include <stdio.h>
#include <stdlib.h>

// Logic #struct_book
typedef struct book {
	int id;
	char name[40];
	int price;
}book_t;

// Logic #meging_sort
void merge_sort(book_t arr[], int left, int right) {
	int mid, i, j, k, n;
	book_t *temp;
	// Logic #check_partision
	if(left == right || left > right)
		return;
	// Logic #find_middle_of_array
	mid = (left + right) / 2;
	// Logic #sort_left_partition 
	merge_sort(arr, left, mid);
	// Logic #sort_the_right_partition 
	merge_sort(arr, mid+1, right);
	// Logic #create_temp_array_to_accomodate_partition
	n = right - left + 1;
	temp = (book_t*) malloc(n * sizeof(book_t));
	// Logic #take_index 
	i = left;
	j = mid+1;
	k = 0;
	while(i <= mid && j <= right) {
		// Logic #compare_elements
		if(arr[i].price > arr[j].price) {
			temp[k] = arr[i];
			i++;
			k++;
		}
		else {
			temp[k] = arr[j];
			j++;
			k++;
		}
	} 
	// Logic #copy_remaining_partition_into_temp_array
	while(i <= mid) {
		temp[k] = arr[i];
		i++;
		k++;
	}
	while(j <= right) {
		temp[k] = arr[j];
		j++;
		k++;
	}
	// Logic #overwrite_temp_array_on_original_array
	for(i=0; i<n; i++)
		arr[left + i] = temp[i];
	// Logic #free_the_temp_array
	free(temp);
}

int main() {
    // Declaration
	book_t arr[10] = {
		{7, "Atlas Shrugged", 734}, 
		{1, "The Alchemist", 623}, 
		{5, "The Fountainhead", 532},
		{3, "Wings of Fire", 325},
		{4, "Yugandhar", 587},
		{8, "Mrityunjay", 973},
		{9, "Rich dad & Poor dad", 534},
		{10, "Monk who sold his ferrari", 238},
		{6, "Chhava", 592},
		{2, "The secret", 351}
	};
	int i, len = 10;

    // Logic #meging_sort
	merge_sort(arr, 0, len-1);

    // Output
	for(i=0; i<len; i++)
		printf("%d, %s, %d\n", arr[i].id, arr[i].name, arr[i].price);
	printf("\n");

	return 0;
}