/*Input employee information from the user including his employee id, name, address, salary, birth date and
date of joining. Find the age of person when he joined company (in years) and his experience till date 
(in months). Also print the date when his probation period is over, assuming that probation period is of 
90 days from date of joining.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define USER_DB "users.db"

 // Declarations 
 typedef struct date {
	int day;
	int month;
	int year;
 }date_t;

typedef struct employee{
    int empId;
    char empName[40];
    char address[80];
    double salary; 
    date_t birth_date;
    date_t join_date;
}employee_t;


// Date Functions
void date_accept(date_t *d);
void date_print(date_t *d);

// User Functions
void accept_employee(employee_t *e);
void display_employee(employee_t *e);

// Input #date
void date_accept(date_t *d) {
	scanf("%d%d%d", &d->day, &d->month, &d->year);
}

// Output #date
void date_print(date_t *d) {
	printf("%d-%d-%d\n", d->day, d->month, d->year);
}

// Input #Employee
void accept_employee(employee_t *e){
    printf("EmpId\t: ");
    scanf("%d",&e->empId);

    printf("Name\t: ");
    scanf("%s",&e->empName);

    printf("Address\t: ");
    scanf("%s",&e->address);

    printf("DOB\t: ");
    date_accept(&e->birth_date);

    printf("Join Date\t: ");
    date_accept(&e->join_date);
}

// Output #user_info
 void display_employee(employee_t *e) {
     printf("ID : %d\t, NAME : %s\t, Address : %s\t,", e->empId, e->empName, e->address);
     printf("DOB : ");
     date_print(&e->birth_date);
     printf("Join Date : ");
     date_print(&e->join_date);
 }

// Logic #checking_leap_or_not
int is_leap(int year) {
	if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
		return 1;
	return 0;
}

// Logic #Get days in month
int date_to_days(date_t *d)
{
   int monthdays[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};
   if(is_leap(d->year))
   {
    monthdays[2]=29;
   }
  int i,calculated_days = d->day;
   for(i=0; i < d->month; i++)
   {
     calculated_days = calculated_days + monthdays[i];
     
   }
 return calculated_days;
}

// Logic #Date compare
int dateCompare(date_t *d,date_t *dc)
{
  int diff,first_date_days,second_date_days,year_diff;
  year_diff=dc->year - d->year;
  if(d->year == dc->year)
        {  
        first_date_days = date_to_days(d);
        second_date_days = date_to_days(dc);
        diff = second_date_days - first_date_days;
        }
  if(dc->year - d->year == 1)
    {
        if(is_leap( d->year ))
        {
          first_date_days = 366 - date_to_days(d);
        }
        else
        {
          first_date_days = 365 - date_to_days(d);
        }
        
      second_date_days = date_to_days(dc);
      diff = second_date_days + first_date_days;
    }
  if(year_diff > 1)
  {
    if(is_leap(d->year))
      {
       first_date_days = 366 - date_to_days(d);
      }
    else
      {
          first_date_days = 365 - date_to_days(d);
      }
    second_date_days = date_to_days(dc);
    diff = second_date_days + first_date_days;
    
    int i,year = d->year;
    for(i=1; i < year_diff; i++)
    {
      year++;
      
      if(is_leap(year))
      {
        diff=diff+366;
      }
      else
      {
        diff=diff+365;
      }
    }
    
  }
return diff;
}

int month_days(int month,int year)
{
   int monthdays[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};
   if(is_leap(year))
   {
       monthdays[2]=29;
   }
   return monthdays[month];
}

// Logic #Add days
date_t addDays(date_t *d,int days)
{
    int i;
    date_t res_date;
    
    res_date.day = d->day;
    res_date.month = d->month;
    res_date.year = d->year;
    for(i=1; i <= days;i++)
    {
    res_date.day++;
    
    if(res_date.day > month_days(res_date.month, res_date.year))
        {    
          res_date.month++;
          res_date.day=1;
            if(res_date.month == 13)
            {
                res_date.month = 1;
                res_date.year++;
               
            }
         
        }
    }
   
    return res_date;
}

int main(){
    employee_t e;
    accept_employee(&e);
    printf("Employee details for %s : \n",&e.empName);
    display_employee(&e);

    int diff = dateCompare(&e.birth_date, &e.join_date);
    printf("Age at the time of joining : %d",diff/365);

    date_t prob_end_date = addDays(&e.join_date,90);
    printf("\nProbation period will end at : ");
    date_print(&prob_end_date);
    
}