#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    // Declaration
    char s1[40] = "Vrushali";
    char s2[15];

    // Logic #String_copy
    strcpy(s2, "Kulkarni");
    printf("%s\n",s2);

    // Logic #string_compare
    if(strcmp(s1,s2) == 0) 
        printf("strings are equal.\n");
    else 
        printf("strings are not equal.\n");

    // Logic #string_reverse
    printf("%s\n",strrev(s1));

    // Logic #joining_strings
    strcat(s1,s2);
    printf("%s\n",s1);

    return 0;
}