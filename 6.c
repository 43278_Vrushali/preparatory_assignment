#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Logic #stuct_student
typedef struct student {
	int roll;
	char name[30];
	int std;
    char subjects[6][20];
	int marks[6];
}student_t;

// Input #student_info
void accept_student(student_t *s) {
    // Declaration
    int i;

	printf("roll: ");
	scanf("%d", &s->roll);
	printf("name: ");
	scanf("%s", s->name);

    printf("std: ");
	scanf("%d", &s->std);

    for(i=0;i<6;i++) {
        printf("Subject: ");
        scanf("%s", &s->subjects[i][20]);

        printf("marks: ");
        scanf("%d", &s->marks[i]);
    }
}

// Output #student_info
void display_student(student_t *s) {
    // Declaration
    int i;

	printf("Roll=%d\nname=%s\nstd=%d\n", s->roll, s->name, s->std);
    for(i=0;i<6;i++) {
        printf("Subject=%s\n", s->subjects[i]);
        printf("Marks=%d\n", s->marks[i]);
    }
}

#define SIZE 5

// Logic #circle_quee
typedef struct cirque {
	student_t arr[SIZE];
	int rear;
	int front;
	int count;
}cirque_t;

// Initailse #quee
void cq_init(cirque_t *q) {
	memset(q->arr, 0, sizeof(q->arr));
	q->front = -1;
	q->rear = -1;
	q->count = 0;
}

// Logic #push_data
void cq_push(cirque_t *q, student_t s) {
	q->rear = (q->rear+1) % SIZE;
	q->arr[q->rear] = s;
	q->count++;
}

// Logic #pop_data
void cq_pop(cirque_t *q) {
	q->front = (q->front+1) % SIZE;	
	q->count--;
}

// Logic #return_next_to_front
student_t cq_peek(cirque_t *q) {
	int index = (q->front+1) % SIZE;
	return q->arr[index];
}

// Logic #check_quee_is_empty
int cq_empty(cirque_t *q) {
	return q->count == 0;
}

// Logic #check_quee_is_full
int cq_full(cirque_t *q) {
	return q->count == SIZE;
}

int main(void) {
    // Declaration
    cirque_t q;
	student_t temp;
	int choice;
	cq_init(&q);

    // Logic #main_menu
	do {
		printf("\n0. exit\n1. push\n2. pop\n3. peek\nenter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // push
			if(cq_full(&q))
				printf("queue is full.\n");
			else {
				accept_student(&temp);
				cq_push(&q, temp);
			}
			break;
		case 2: // pop
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				cq_pop(&q);
				printf("popped: ");
				display_student(&temp);
			}
			break;
		case 3: // peek
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				printf("topmost: ");
				display_student(&temp);
			}
			break;
		}
	} while(choice != 0);

    return 0;
}